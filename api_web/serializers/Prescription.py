from api_web.serializers.Clinic import ClinicSerializer
from api_web.serializers.Patient import PatientSerializer
from api_web.serializers.Physician import PhysicianSerializer
from core.serializers import BaseSerializer
from domain.entity.Prescription import Prescription
from rest_framework import serializers


class PrescriptionSerializer(BaseSerializer):
    clinic = ClinicSerializer(many=False, help_text='Clínica responsável pela prescrição')
    patient = PatientSerializer(many=False, help_text='Paciente prescrito')
    physician = PhysicianSerializer(many=False, help_text='Médico responsável pela prescrição')
    text = serializers.CharField(help_text='Detalhes da prescrição', allow_null=False, required=True)

    class Meta:
        model = Prescription
        fields = ['id', 'text', 'clinic', 'patient', 'physician', 'create_date']

    def create(self, validated_data):
        clinic = validated_data.pop('clinic')
        patient = validated_data.pop('patient')
        physician = validated_data.pop('physician')

        prescription = Prescription.objects.create(
            **validated_data, clinic_id=clinic['id'], patient_id=patient['id'], physician_id=physician['id']
        )
        return prescription

    def update(self, instance, validated_data):
        clinic = validated_data.pop('clinic')
        patient = validated_data.pop('patient')
        physician = validated_data.pop('physician')

        instance.clinic_id = clinic['id']
        instance.patient_id = patient['id']
        instance.physician_id = physician['id']
        instance.text = validated_data.get('text')
        instance.save()
        return instance
