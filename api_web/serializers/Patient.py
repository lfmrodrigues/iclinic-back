from core.serializers import BaseSerializer
from domain.entity.Patient import Patient
from rest_framework import serializers


class PatientSerializer(BaseSerializer):
    name = serializers.CharField(required=False, help_text='Nome do paciente')

    class Meta:
        model = Patient
        fields = ['id', 'name', 'create_date']
