"""iclinic_back URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from api_web import router
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions

schema_view = get_schema_view(
    openapi.Info(
        title="Documentação Teste Técnico IClinic",
        default_version='v1',
        description='''

        Esta documentação tem o objetivo de descrever os *endpoints* da API do teste técnico para a IClinic.<br><br>
        
        Este projeto foi desenvolvido sob Python 3.6.9 no sistema operacional Linux Mint (distribuição do Ubuntu).<br>
        A versão do Django utilizada é a 2.2.9.
        
        Para simplificar a implantação deste sistema backend, todas as APIs estão abertas, ou seja, não exigem um método de autorização e permissão.
        <br><br>
        
        Todos os métodos que implementam a listagem de um determinado recurso, entrega a resposta de forma paginada, com 15 itens por página.
        <br><br>
        Outra forma para simplificar a implementação, é a utilização de um banco SQLite para persistir os dados. Este banco já foi populado com 
        alguns dados de teste durante o desenvolvimento e que podem ser utilizados para validar a implementação aqui realizada.<br>
        No diretório principal deste projeto, encontra-se a DER o banco modelado.
        <br><br>
        
        

        ''',
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('api/v1/', include(router.router.urls)),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('admin/', admin.site.urls),
]
