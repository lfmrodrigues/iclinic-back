from django.db import models

from core.entity.Audit import Audit


class Clinic(Audit):
    name = models.CharField(max_length=255, null=False)
