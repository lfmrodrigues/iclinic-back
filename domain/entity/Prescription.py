from django.db import models

from core.entity.Audit import Audit


class Prescription(Audit):
    text = models.TextField(null=True)
    clinic = models.ForeignKey('domain.Clinic', on_delete=models.PROTECT)
    physician = models.ForeignKey('domain.Physician', on_delete=models.PROTECT)
    patient = models.ForeignKey('domain.Patient', on_delete=models.PROTECT)
