from rest_framework import serializers

from core.serializers import BaseSerializer
from domain.entity.Clinic import Clinic


class ClinicSerializer(BaseSerializer):
    name = serializers.CharField(required=False, help_text='Nome da clínica')

    class Meta:
        model = Clinic
        fields = ['id', 'name', 'create_date']
