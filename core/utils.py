from email.mime.image import MIMEImage
from pathlib import Path
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from iclinic_back.settings import BASE_DIR


def send_email_with_image(title=None, template=None, params=None, recipient=None):
    email = EmailMultiAlternatives(subject=title, from_email='', to=recipient)
    body = render_to_string(template, params)

    logo_path = BASE_DIR + '/core/templates/img/logo.png'
    logo_name = Path(logo_path).name

    if all([body, logo_path, logo_name]):
        email.attach_alternative(body, "text/html")
        email.content_subtype = 'html'
        email.mixed_subtype = 'related'

        with open(logo_path, mode='rb') as f:
            image = MIMEImage(f.read())
            image.add_header('Content-ID', f"<{logo_name}>")
            email.attach(image)

    email.send()