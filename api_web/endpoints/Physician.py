from rest_framework.permissions import AllowAny

from api_web.serializers.Physician import PhysicianSerializer
from core.viewset import BaseViewSet
from domain.entity.Physician import Physician


class PhysicianViewSet(BaseViewSet):
    """
    list:
    Recupera a lista de médicos cadastrados<br>
    **&#9744; Requer Autenticação**
    **&#9745; Suporta Paginação com 15 resultados/página** - Para ativar a paginação, utilize o parâmetro &page=INT como *querystring* na requisição.
    **&#9745; Suporta alteração do tamanho do resultado de uma página** - Utilize o parâmetro &itensPerPage=INT para modificar o tamanho do resultado de uma determinada página.
    **&#9744; Suporta seleção de campos** - Utilize o parâmetro &delete=NOME_CAMPO como *querystring* na requisição. **OBS: Substitua o padrão *Cammel Case* por *underscore* no nome do campo**
    **&#9745; Suporta ordenação** - Utilize o parâmetro &orderBy=NOME_CAMPO (ascendente) ou  &orderBy=-NOME_CAMPO (descendente) como *querystring* na requisição para especificar uma ordenação. **OBS: Substitua o padrão *Cammel Case* por *underscore* no nome do campo**

    create:
    Cria uma novo médico.
    **&#9744; Requer Autenticação**<br><br>

    retrieve:
    Recupera um médico pelo ID especificado.
    **&#9744; Requer Autenticação**<br><br>

    update:
    Atualiza o médico pelo ID especificado.
    **&#9744; Requer Autenticação**<br><br>

    destroy:
    Deleta um médico.
    **&#9744; Requer Autenticação**<br><br>

    partial_update:
    Atualiza o médico pelo ID especificado.
    **&#9744; Requer Autenticação**<br><br>
    """
    permission_classes = (AllowAny,)
    queryset = Physician.objects.all()
    serializer_class = PhysicianSerializer
