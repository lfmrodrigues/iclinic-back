from rest_framework.response import Response
from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)

    try:
        if str(exc) == 'No active account found with the given credentials':
            errorCode = 1

        elif 'code=\'unique\'' in str(exc):
            errorCode = 100

        elif 'non_field_errors' in response.data and str(
                response.data['non_field_errors'][0]) == 'The fields email must make a unique set.':
            errorCode = 4

        elif 'non_field_errors' in response.data and str(
                response.data['non_field_errors'][0]) == 'The fields cpf must make a unique set.':
            errorCode = 5

        else:
            key = list(dict(response.data).keys())[0]
            errorCode = int(response.data[key]['errorCode'])

        return Response({'errorCode': errorCode}, status=400)

    except Exception as e:
        if response is not None:
            response.data['status_code'] = response.status_code

        return response
