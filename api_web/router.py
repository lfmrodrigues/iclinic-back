from rest_framework import routers

from api_web.endpoints.Clinic import ClinicViewSet
from api_web.endpoints.Patient import PatientViewSet
from api_web.endpoints.Physician import PhysicianViewSet
from api_web.endpoints.Prescription import PrescriptionViewSet

router = routers.DefaultRouter(trailing_slash=True)
urlpatterns = router.urls

router.register(r'clinics', ClinicViewSet)
router.register(r'patients', PatientViewSet)
router.register(r'physicians', PhysicianViewSet)
router.register(r'prescriptions', PrescriptionViewSet)
