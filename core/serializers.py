from rest_framework import serializers


class BaseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, allow_null=True, help_text='Identificador do registro')
    create_date = serializers.DateTimeField(read_only=True, help_text='Data de criação do registro')

    def __init__(self, *args, **kwargs):
        remove_fields = kwargs.pop('remove_fields', None)
        super(BaseSerializer, self).__init__(*args, **kwargs)

        if remove_fields:
            for field in remove_fields:
                if '.' in field:
                    try:
                        try:
                            self.fields[field.split('.')[0]].fields.pop(field.split('.')[1])
                        except:
                            pass
                    except:
                        try:
                            self.fields[field.split('.')[0]].child.fields.pop(field.split('.')[1])
                        except:
                            pass
                else:
                    self.fields.pop(field)
