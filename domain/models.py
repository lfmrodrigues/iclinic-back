from domain.entity.Clinic import Clinic
from domain.entity.Patient import Patient
from domain.entity.Physician import Physician
from domain.entity.Prescription import Prescription