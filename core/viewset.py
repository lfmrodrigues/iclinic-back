from rest_framework import viewsets
from rest_framework.response import Response

from core.pagination import CustomPagination


class BaseViewSet(viewsets.ModelViewSet):
    def list(self, request, *args, **kwargs):
        qs = self.get_queryset()
        remove_fields = self.request.query_params.getlist('delete', [])
        orderBy = self.request.query_params.get('orderBy', '-create_date')
        no_paginate = self.request.query_params.get('noPaginate', None)
        itens_per_page = self.request.query_params.get('itensPerPage', 15)

        if no_paginate and no_paginate == '1':
            serializer = self.serializer_class(qs.order_by(orderBy), remove_fields=remove_fields, many=True)
            return Response(serializer.data)

        else:
            custom_pagination = CustomPagination()
            custom_pagination.page_size = int(itens_per_page)
            result = custom_pagination.paginate_queryset(qs.order_by(orderBy), request)
            serializer = self.serializer_class(result, remove_fields=remove_fields, many=True)
            return custom_pagination.get_paginated_response(serializer.data)

    def get_paginated_data(self, qs, serializer_class, request):
        custom_pagination = CustomPagination()
        result = custom_pagination.paginate_queryset(qs, request)
        data = serializer_class(result, many=True).data
        return custom_pagination.get_paginated_response(data)
