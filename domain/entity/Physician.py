from django.db import models

from core.entity.Audit import Audit


class Physician(Audit):
    name = models.CharField(max_length=255, null=False)
