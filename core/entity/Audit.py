from django.db import models


class Audit(models.Model):
    create_date = models.DateTimeField(auto_now_add=True, null=True)
    last_update_date = models.DateTimeField(auto_now=True, null=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True
