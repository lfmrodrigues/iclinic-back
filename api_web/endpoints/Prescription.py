from rest_framework.permissions import AllowAny

from api_web.serializers.Prescription import PrescriptionSerializer
from core.viewset import BaseViewSet
from domain.entity.Prescription import Prescription


class PrescriptionViewSet(BaseViewSet):
    """
            list:
            Recupera a lista de prescrições cadastradas<br>
            **&#9744; Requer Autenticação**
            **&#9745; Suporta Paginação com 15 resultados/página** - Para ativar a paginação, utilize o parâmetro &page=INT como *querystring* na requisição.
            **&#9745; Suporta alteração do tamanho do resultado de uma página** - Utilize o parâmetro &itensPerPage=INT para modificar o tamanho do resultado de uma determinada página.
            **&#9744; Suporta seleção de campos** - Utilize o parâmetro &delete=NOME_CAMPO como *querystring* na requisição. **OBS: Substitua o padrão *Cammel Case* por *underscore* no nome do campo**
            **&#9745; Suporta ordenação** - Utilize o parâmetro &orderBy=NOME_CAMPO (ascendente) ou  &orderBy=-NOME_CAMPO (descendente) como *querystring* na requisição para especificar uma ordenação. **OBS: Substitua o padrão *Cammel Case* por *underscore* no nome do campo**

            create:
            Cria uma nova prescrição.
            **&#9744; Requer Autenticação**<br><br>

            retrieve:
            Recupera uma prescrição pelo ID especificado.
            **&#9744; Requer Autenticação**<br><br>

            update:
            Atualiza a prescrição pelo ID especificado.
            **&#9744; Requer Autenticação**<br><br>

            destroy:
            Deleta uma prescrição.
            **&#9744; Requer Autenticação**<br><br>

            partial_update:
            Atualiza a prescrição pelo ID especificado.
            **&#9744; Requer Autenticação**<br><br>
            """
    permission_classes = (AllowAny,)
    queryset = Prescription.objects.all()
    serializer_class = PrescriptionSerializer
