from django.test import TestCase

# Create your tests here.
from rest_framework.test import APITestCase
from rest_framework import status


class ClinicTest(APITestCase):
    url = 'http://localhost:8000/api/v1/clinics/'

    def test_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        data = {'name': 'Clínica Teste'}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        data = {'name': 'Clínica Beta'}
        response = self.client.post(self.url, data, format='json')

        data_id = str(response.data['id'])
        data = {'id': int(data_id), 'name': 'Clínica Alfa'}
        response = self.client.put(self.url + data_id + '/', data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(str(response.data['id']), data_id)
        self.assertEqual(response.data['name'], 'Clínica Alfa')


class PatientTest(APITestCase):
    url = 'http://localhost:8000/api/v1/patients/'

    def test_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        data = {'name': 'Wilma Ortega'}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        data = {'name': 'Josealdo Ribeiro'}
        response = self.client.post(self.url, data, format='json')

        data_id = str(response.data['id'])
        data = {'id': int(data_id), 'name': 'Vera Silva'}
        response = self.client.put(self.url + data_id + '/', data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(str(response.data['id']), data_id)
        self.assertEqual(response.data['name'], 'Vera Silva')


class PhysicianTest(APITestCase):
    url = 'http://localhost:8000/api/v1/physicians/'

    def test_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create(self):
        data = {'name': 'Dr. Emanuel Braga Neto'}
        response = self.client.post(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        data = {'name': 'Silvia Moreira'}
        response = self.client.post(self.url, data, format='json')

        data_id = str(response.data['id'])
        data = {'id': int(data_id), 'name': 'André Peixoto'}
        response = self.client.put(self.url + data_id + '/', data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(str(response.data['id']), data_id)
        self.assertEqual(response.data['name'], 'André Peixoto')


class PrescriptionTest(APITestCase):
    url = 'http://localhost:8000/api/v1/prescriptions/'

    def test_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
