from core.serializers import BaseSerializer
from domain.entity.Physician import Physician
from rest_framework import serializers


class PhysicianSerializer(BaseSerializer):
    name = serializers.CharField(required=False, help_text='Nome do médico')

    class Meta:
        model = Physician
        fields = ['id', 'name', 'create_date']
