# Teste Técnico para IClinic

### Nome: Luiz Fernando Moreira Rodrigues

Este projeto foi implementado como teste técnico para a IClinic.
Foi desenvolvido sob a versão 3.6.9 do Python no Linux Mint e framework Django 2.2.9.

O banco de dados utilizado foi o SQLite de modo a simplificar o desenvolvimento e conferência da IClinic. Uma vez que este banco é embarcado no Python. 

Foram implementadas 4 APIs distintas: 

- Physician: /api/v1/physicians/
- Patient: /api/v1/patients/
- Clinic: /api/v1/clinics/
- Prescription: /api/v1/prescription/

Uma vez que a API Prescription depende de dados relacionados das outras APIs, estes dados primeiramente devem ser cadastrados para cadastrar uma prescrição. 

## Rodando o projeto

Todas as dependências do projeto, estão listadas no arquivos requirements.txt, na pasta raiz do projeto.
Portanto, primeiramente, instale as dependências através do comando e localizado na pasta raiz: 
- pip install -r requirements.txt

Por fim, execute o comando para rodar a aplicação: 
- python manage.py runserver 

Este último comando irá servir a aplicação no endereço e porta localhost:8000

Uma documentação em Swagger está disponível no endereço: **http://localhost:8000/docs/**

